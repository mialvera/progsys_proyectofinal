#include <arpa/inet.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "csapp.h"
#include <sys/mman.h>
#include <nodoDevice.h>
#include <procesarDv.h>

#include <ulfius.h>
#include <yder.h>
#include <orcania.h>
#include <jansson.h>
#include <syslog.h>

#define U_DISABLE_JANSSON
#define U_DISABLE_CURL
#define U_DISABLE_WEBSOCKET

#define PORT 2884
#define PREFIX "/auth"

#define USER "devices"
#define PASSWORD "devpass"

#define MAXLINE_CLIENT 1024 * 1024

/*
 * Función que carga los dispositivos obtenidos a partir del Monitor USB
 * Retorna la informacion de los dispositivos en formato JSON
 */
char * cargarDevices();

/*
 * Función que carga los archivos existentes en el directorio raíz a partir de un dispositivo
 * Retorna el listado de los archivos existentes en el dispositivo en formato JSON
 */
char * cargarFiles(char * value);

// CALLBACKS

/*
 * Trabajará con el endpoint /devices - METODO GET
 * RESPONSE -> HTTP CODE (200 - OK)
 */
int callback_devices (const struct _u_request * request, struct _u_response * response, void * user_data);

/*
 * Trabajará con el endpoint /archivos - METODO POST
 * RESPONSE -> HTTP CODE (200 - OK)
 */
int callback_archivos (const struct _u_request * request, struct _u_response * response, void * user_data);

/*
 * Trabajará con el endpoint /archivos/eliminar - METODO DELETE
 * RESPONSE -> HTTP CODE (404 -> Archivo no encontrado, Dispositivo no válido)
 * RESPONSE -> HTTP CODE (200 - OK -> Archivo eliminado con éxito)
 */
int callback_borrar (const struct _u_request * request, struct _u_response * response, void * user_data);

/*
 * Trabajará con el endpoint /archivos/crear - METODO POST
 * RESPONSE -> HTTP CODE (500 -> Dispositivo no válido)
 * RESPONSE -> HTTP CODE (200 - OK -> Archivo creado con éxito)
 */
int callback_crear (const struct _u_request * request, struct _u_response * response, void * user_data);

char * print_map(const struct _u_map * map) {
  char * line, * to_return = NULL;
  const char **keys;
  int len, i;
  if (map != NULL) {
    keys = u_map_enum_keys(map);
    for (i=0; keys[i] != NULL; i++) {
      len = snprintf(NULL, 0, "%s: %s\n", keys[i], u_map_get(map, keys[i]));
      line = o_malloc((len+1)*sizeof(char));
      snprintf(line, (len+1), "%s: %s\n", keys[i], u_map_get(map, keys[i]));
      if (to_return != NULL) {
        len = strlen(to_return) + strlen(line) + 1;
        to_return = o_realloc(to_return, (len+1)*sizeof(char));
      } else {
        to_return = o_malloc((strlen(line) + 1)*sizeof(char));
        to_return[0] = 0;
      }
      strcat(to_return, line);
      o_free(line);
    }
    return to_return;
  } else {
    return NULL;
  }
}

int callback_devices (const struct _u_request * request, struct _u_response * response, void * user_data) {
    
  char * url_params, *headers, * cookies, *post_params;
  url_params = print_map(request->map_url);
  headers = print_map(request->map_header);
  cookies = print_map(request->map_cookie);
  post_params = print_map(request->map_post_body);
  char request_body[request->binary_body_length + 1];
  strncpy(request_body, request->binary_body, request->binary_body_length);
  request_body[request->binary_body_length] = '\0';

  json_t *body;
  json_error_t error;
  char * text_devices = cargarDevices();
  body = json_loads((const char *)text_devices, 0, &error);
  ulfius_set_json_body_response(response,200, body);


  printf("######################################################\n");
  printf("############ Callback (Listar Dispositivos) ##############\n");
  printf("######################################################\n\n");
  printf("Method is %s\n", request -> http_verb); 
  printf("url is %s\n\n", request -> http_url);
  printf("parameters from the url are \n%s\n\n", url_params);
  printf("cookies are \n%s\n\n", cookies);
  printf("headers are \n%s\n\n", headers);  
  printf("post parameters are \n%s\n\n", post_params);
  printf("raw body is \n%s\n\n", request_body);
  printf("user data is %s\n\n", (char *)user_data);
  
  ulfius_add_cookie_to_response(response, "cook", "ie", NULL, 5000, NULL, NULL, 0, 0);
  u_map_put(response->map_header, "multiple-key", "value 1");
  u_map_put(response->map_header, "Multiple-Key", "value 2");

  free(text_devices);

  return U_CALLBACK_CONTINUE;
}

int callback_archivos (const struct _u_request * request, struct _u_response * response, void * user_data) {
    
  char * url_params;
  url_params = print_map(request->map_url);
  char request_body[request->binary_body_length + 1];
  memcpy(request_body, request->binary_body, request->binary_body_length);
  request_body[request->binary_body_length] = '\0';


  printf("######################################################\n");
  printf("############ Callback (Listar Archivos) ##############\n");
  printf("######################################################\n\n");
  printf("Method is %s\n", request -> http_verb); 
  printf("url is %s\n\n", request -> http_url);
  printf("parameters from the url are \n%s\n\n", url_params);
  printf("raw body is \n%s\n\n", request_body);
  printf("user data is %s\n\n", (char *)user_data);

  struct jReadElement result;

  jRead( request_body, "{'nodo'", &result );

  char *nodo = result.pValue;

  json_t *body;
  json_error_t error;
  char * text_archivos = cargarFiles(nodo);
  body = json_loads(text_archivos, 0, &error);
  ulfius_set_json_body_response(response,200, body);

  ulfius_add_cookie_to_response(response, "cook", "ie", NULL, MAXLINE_CLIENT, NULL, NULL, 0, 0);
  u_map_put(response->map_header, "multiple-key", "value 1");
  u_map_put(response->map_header, "Multiple-Key", "value 2");
  
  free(text_archivos);
  return U_CALLBACK_CONTINUE;

}


int callback_borrar (const struct _u_request * request, struct _u_response * response, void * user_data){

  char * url_params, *headers, * cookies, *post_params;
  url_params = print_map(request->map_url);
  headers = print_map(request->map_header);
  cookies = print_map(request->map_cookie);
  post_params = print_map(request->map_post_body);
  char request_body[request->binary_body_length + 1];
  memcpy(request_body, request->binary_body, request->binary_body_length);
  request_body[request->binary_body_length] = '\0';

  printf("######################################################\n");
  printf("############# Callback (Borrar Archivo) ##############\n");
  printf("######################################################\n\n");
  printf("Method is %s\n", request -> http_verb); 
  printf("url is %s\n\n", request -> http_url);
  printf("parameters from the url are \n%s\n\n", url_params);
  printf("cookies are \n%s\n\n", cookies);
  printf("headers are \n%s\n\n", headers);  
  printf("post parameters are \n%s\n\n", post_params);
  printf("raw body is \n%s\n\n", request_body);
  printf("user data is %s\n\n", (char *)user_data);

  json_error_t error;
  json_t *data, *nodoObject, *archivoObject;
  data = json_loads(request_body, 0, &error);

  nodoObject = json_object_get(data, "nodo");
  const char *nodo = json_string_value(nodoObject);

  char *montaje = getMontaje(strdup(nodo));
  
  int longitud, status;

  longitud = strlen(montaje);

  char *mensaje;

  free(response -> binary_body);
  mensaje = (char *)calloc(100, sizeof(char));
  if (longitud == 0) {
    strcat(mensaje, "Dispositivo no encontrado");
    status = 404;
  }
  else {

    archivoObject = json_object_get(data, "archivo");
    const char *archivo = json_string_value(archivoObject);

    char mont[50];
    strcpy(mont,montaje);
    strcat(mont, " ");

    borrarFile(mont, strdup(archivo));

    char *fname = (char *)calloc(15, sizeof(char));

    strcat(fname, "error_borrado.txt");

    if( access( fname, F_OK ) != -1 ) {
        
        printf("Archivo existe\n");
        borrarError(fname);
        status = 404;
        strcat(mensaje, "Archivo no existe");

    } else {

      status = 200;
      strcat(mensaje, "Archivo ha sido borrado con éxito");

    }

  }

  response -> binary_body = strdup(mensaje);
  response -> binary_body_length = strlen(mensaje);
  response -> status = status;

  ulfius_add_cookie_to_response(response, "cook", "ie", NULL, MAXLINE_CLIENT, NULL, NULL, 0, 0);
  u_map_put(response->map_header, "multiple-key", "value 1");
  u_map_put(response->map_header, "Multiple-Key", "value 2");
  
  return U_CALLBACK_CONTINUE;

}

int callback_crear (const struct _u_request * request, struct _u_response * response, void * user_data){

  char * url_params, *headers, * cookies, *post_params;
  url_params = print_map(request->map_url);
  headers = print_map(request->map_header);
  cookies = print_map(request->map_cookie);
  post_params = print_map(request->map_post_body);
  char request_body[request->binary_body_length + 1];
  memcpy(request_body, request->binary_body, request->binary_body_length);
  request_body[request->binary_body_length] = '\0';

  printf("######################################################\n");
  printf("############## Callback (Crear Archivo) ##############\n");
  printf("######################################################\n\n");
  printf("Method is %s\n", request -> http_verb); 
  printf("url is %s\n\n", request -> http_url);
  printf("parameters from the url are \n%s\n\n", url_params);
  printf("cookies are \n%s\n\n", cookies);
  printf("headers are \n%s\n\n", headers);  
  printf("post parameters are \n%s\n\n", post_params);
  printf("raw body is \n%s\n\n", request_body);
  printf("user data is %s\n\n", (char *)user_data);

  json_error_t error;
  json_t *data, *nodoObject, *archivoObject, *contenidoObject;
  data = json_loads(request_body, 0, &error);

  nodoObject = json_object_get(data, "nodo");
  const char * nodo = json_string_value(nodoObject);

  char *montaje = getMontaje(strdup(nodo));
  
  int longitud, status;

  longitud = strlen(montaje);

  char *mensaje;


  free(response -> binary_body);
  mensaje = (char *)calloc(100, sizeof(char));
  if (longitud == 0) {
    strcat(mensaje, "Dispositivo no encontrado");
    status = 500;
  }
  else {

    archivoObject = json_object_get(data, "archivo");
    const char *archivo = json_string_value(archivoObject);

    contenidoObject = json_object_get(data, "contenido");
    const char * contenido = json_string_value(contenidoObject);

    char mont[50];
    strcpy(mont,montaje);
    strcat(mont, " ");

    crearFile(montaje, strdup(archivo), strdup(contenido));

    status = 200;
    strcat(mensaje, "Archivo ha sido creado con éxito");
      
  }

  response -> binary_body = strdup(mensaje);
  response -> binary_body_length = strlen(mensaje);
  response -> status = status;

  ulfius_add_cookie_to_response(response, "cook", "ie", NULL, MAXLINE_CLIENT, NULL, NULL, 0, 0);
  u_map_put(response->map_header, "multiple-key", "value 1");
  u_map_put(response->map_header, "Multiple-Key", "value 2");
  
  return U_CALLBACK_CONTINUE;
}


char * cargarDevices(){

  char * text_devices = (char *)malloc(MAXLINE_CLIENT);

  int clientfd;
  char *filename;
  char *port;
  char *host, buf[MAXLINE_CLIENT];

  host = "127.0.0.1";
  port = "9090";
  filename = "dispositivos.json";

  clientfd = Open_clientfd(host, port);

  //Se envia el nombre del archivo que es el tercer argumento de ejecutable
  write(clientfd, filename, strlen(filename));

  shutdown(clientfd, SHUT_WR);

  //lee cuantos bytes tiene el archivo y lo imprime
  size_t  get = read(clientfd, buf, MAXLINE_CLIENT - 1);
      
  if (strcmp(buf,"-1") != 0) {
    buf[get] = '\0';
    fprintf(stdout, "File:  %s bytes\n", buf);

    int textsize = atoi(buf);

    if(textsize >= 0){

      if(textsize > 0){

        get = read(clientfd, text_devices, textsize + 1);

        text_devices[textsize] = '\0';

      }

      printf("Dispositivos recibidos: %s\n", text_devices);

    }
  }

  else {
      printf("Archivo no encontrado\n");
  }
  close(clientfd);

  return text_devices;

}

char * cargarFiles( char * value){

  char *montaje = getMontaje(strdup(value));

  printf("Direccion de montaje %s\n", montaje);
  
  listarFilesDevice(montaje);

  listaInfo *lista2;

  lista2 = readFileChar("archivos.txt");

  if( access( "archivos.txt", F_OK ) != -1 )
    borrarError("archivos.txt");

  char * text_archivos = escribirJSONInfo(lista2, "info.json");

  return text_archivos;

}


pid_t process_id = 0;

int main(int argc, char* argv[]) {

  pid_t sid = 0;

  // Crea un proceso hijo
  process_id = Fork();

  if (process_id < 0){

      printf("fork failed!\n");
      exit(1);

  }
  // Detecta al proceso padre y lo termina
  if (process_id > 0)
  {
      syslog(LOG_DEBUG, "process_id of child process %d \n", process_id);
      exit(0);
  }

  umask(0);
  //setea la nueva sesion al hijo
  sid = setsid();
  if(sid < 0) {
    // Return failure
    exit(1);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  struct _u_instance instance;

  y_init_logs("server", Y_LOG_MODE_CONSOLE, Y_LOG_LEVEL_DEBUG, NULL, "logs start");

  if (ulfius_init_instance(&instance, PORT, NULL, NULL) != U_OK) {
      printf("Error ulfius_init_instance, abort\n");
      return(1);
  }
    
  u_map_put(instance.default_headers, "Access-Control-Allow-Origin", "*");


  instance.max_post_body_size = 0;

  
  // ENDPOINTS

  // /devices -> Listará los dispositivos conectados al ordenador
  // /archivos -> Mostrará los archivos existentes (directorio raiz) en un dispositivo USB.
  // /archivos/crear -> Creará un archivo en el dispositivo USB seleccionado
  // /archivos/eliminar -> Eliminará un archivo que será buscado en el dispositivo USB.

  ulfius_add_endpoint_by_val(&instance, "GET", NULL, "/devices", 0, &callback_devices, NULL);       

  ulfius_add_endpoint_by_val(&instance, "POST", NULL, "/archivos", 0, &callback_archivos, NULL);
  
  ulfius_add_endpoint_by_val(&instance, "POST", NULL, "/archivos/crear", 0, &callback_crear, NULL);

  ulfius_add_endpoint_by_val(&instance, "DELETE", NULL, "/archivos/eliminar", 0, &callback_borrar, NULL);



  // Start the framework
  if (ulfius_start_framework(&instance) == U_OK) {
    syslog(LOG_INFO, "Start framework on port %d\n", instance.port);
  
    // Wait for the user to press <enter> on the console to quit the application
    // printf("Press <enter> to quit server\n");
    // getchar();
    while (1) {;}


  } else {
      printf("Error starting framework\n");
  }

  printf("End framework\n");
  ulfius_stop_framework(&instance);
  ulfius_clean_instance(&instance);
  return 0;

}
