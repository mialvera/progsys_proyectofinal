#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

typedef struct FileBuffer {
	unsigned long length;			// length in bytes
	unsigned char *data;			// malloc'd data, free with freeFileBuffer()
} FileBuffer;


unsigned long readFileBuffer(char *filename, FileBuffer *pbuf, unsigned long maxlen);

void freeFileBuffer(FileBuffer *buf);