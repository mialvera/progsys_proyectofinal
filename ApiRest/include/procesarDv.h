#include <stdio.h>
#include <nodoInfo.h>
#include <stdlib.h>
#include <csapp.h>
#include <mntent.h>
#include <string.h>
#include <unistd.h>
#include <nodoChar.h>


#define MAXIMO_LENG 1024

char *getMontaje (char * dev_path);
void *listarFilesDevice(char *montaje);
void setup(char inputBuffer[], char *args[]);
listaInfo * readFileChar(char * fileName);
void *borrarFile(char *montaje, char *fileName);
void crearFile(char * montaje, char *file, char * texto);
void *borrarError(char *filename);
