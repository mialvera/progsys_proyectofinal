#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAXIMO 50
#define FILE_BUFFER_MAXLEN 1024*1024


typedef void (*liberarEspacio2)(char *);

typedef struct nodoChar {

	struct nodoChar *previo;
	char *contenido;
	struct nodoChar *siguiente;

} nodoChar; 


typedef struct listaChar {
	
	nodoChar *inicio;
	nodoChar *fin;
	int tamanioLista;
	int tamanioElemento;
	liberarEspacio2 liberar;

} listaChar;


void crearListaChar(listaChar *l, int tamanioElemento, liberarEspacio2 liberar);

nodoChar * crearNodoChar(listaChar *l, char *contenido);

void insertarCharAlFinal(listaChar *l, char *contenido);

int obtenerCantidadChar(listaChar *l);

bool estaVaciaChar(listaChar *l);

void imprimirListaChar(listaChar *l);

nodoChar *obtenerPrimerChar(listaChar *l);

char * buscarChar(listaChar *l, int numeroChar);

