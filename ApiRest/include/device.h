#include <libudev.h>

#define MAXIMO 50

typedef struct Device {

	const char *SubSystem;
	const char *fabricante;
	const char *tipo;
	const char *accion;
	const char *VI;
	const char *PI;
	const char *nodo;

} Device;

Device * crearDevice(struct udev_device* dev);
void setDatos(Device *d, struct udev_device* dev);
void printDevice(Device *d);
