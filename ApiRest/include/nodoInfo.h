#include <stdio.h>
#include <stdlib.h>
#include <Info.h>
#include <stdbool.h>
//#include <filebuffer.h>

#define MAXIMO 50
#define FILE_BUFFER 1024*1024


typedef void (*liberarEspacioInfo)(Info *);

typedef struct nodoInfo {

	struct nodoInfo *previo;
	Info *info;
	struct nodoInfo *siguiente;

} nodoInfo; 


typedef struct listaInfo {
	
	nodoInfo *inicio;
	nodoInfo *fin;
	int tamanioLista;
	int tamanioElemento;
	liberarEspacioInfo liberar;

} listaInfo;

typedef struct FileBufferInfo {
	unsigned long length;			// length in bytes
	unsigned char *data;			// malloc'd data, free with freeFileBuffer()
} FileBufferInfo;



void crearListaInfo(listaInfo *l, int tamanioElemento, liberarEspacioInfo liberar);

nodoInfo * crearNodoInfo(listaInfo *l, Info *i);

void insertarInfoAlFinal(listaInfo *l, Info *i);

int obtenerCantidadInfo(listaInfo *l);

bool estaVaciaInfo(listaInfo *l);

void imprimirListaInfo(listaInfo *l);

nodoInfo *obtenerPrimerInfo(listaInfo *l);

char * escribirJSONInfo(listaInfo *l, char *file);

unsigned long readFileBufferInfo(char *filename, FileBufferInfo *pbuf, unsigned long maxlen);

void freeFileBufferInfo(FileBufferInfo *buf);

Info * buscarInfo(listaInfo *l, int numeroInfo);

listaInfo * leerJSONInfo(char *nombreArchivo);