#include <stdio.h>
#include <stdlib.h>
#include <device.h>
#include <stdbool.h>

#include <jWrite.h>
#include <jRead.h>

#define MAXIMO 50
#define FILE_BUFFER_MAXLEN 1024*1024


typedef void (*liberarEspacio)(Device *);

typedef struct nodoDevice {

	struct nodoDevice *previo;
	Device *device;
	struct nodoDevice *siguiente;

} nodoDevice; 


typedef struct listaDevices {
	
	nodoDevice *inicio;
	nodoDevice *fin;
	int tamanioLista;
	int tamanioElemento;
	liberarEspacio liberar;

} listaDevices;

typedef struct FileBuffer {
	unsigned long length;			// length in bytes
	unsigned char *data;			// malloc'd data, free with freeFileBuffer()
} FileBuffer;


void crearListaDevices(listaDevices *l, int tamanioElemento, liberarEspacio liberar);

nodoDevice * crearNodoDevice(listaDevices *l, Device *d);

void insertarDeviceAlFinal(listaDevices *l, Device *c);

int obtenerCantidadDevices(listaDevices *l);

bool estaVaciaDevices(listaDevices *l);

void imprimirListaDevices(listaDevices *l);

nodoDevice *obtenerPrimerDevice(listaDevices *l);

void escribirJSON(listaDevices *l, char *file);

unsigned long readFileBuffer(char *filename, FileBuffer *pbuf, unsigned long maxlen);

void freeFileBuffer(FileBuffer *buf);

Device * buscarDevice(listaDevices *l, int numeroDevice);

listaDevices * leerJSON(char *nombreArchivo);

listaDevices * leerJSONFromString(char *infoJSON);