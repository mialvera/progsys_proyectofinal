#include <assert.h>
#include <nodoChar.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>



void crearListaChar(listaChar *l, int tamanioElemento, liberarEspacio2 liberar)
{
	assert(tamanioElemento > 0);

	l -> tamanioLista = 0;

	l -> tamanioElemento = tamanioElemento;

	l -> inicio = l -> fin = NULL;

	l -> liberar = liberar;

}

nodoChar * crearNodoChar(listaChar *l, char *cont) {

	if (l != NULL) {

		nodoChar *nuevoNodo = (nodoChar *) malloc(sizeof(nodoChar));

		nuevoNodo -> contenido = malloc(l -> tamanioElemento);

		memcpy(nuevoNodo -> contenido, cont, l -> tamanioElemento);

		return nuevoNodo;

	}

	return NULL;

}


void insertarCharAlFinal(listaChar *l, char *cont)
{
	if (l != NULL) {

		nodoChar *nuevoNodo = crearNodoDevice(l, cont);

		if (estaVaciaChar(l)) {

			l -> inicio = l -> fin = nuevoNodo;

		}

		else {

			l -> fin -> siguiente = nuevoNodo;

			nuevoNodo -> previo = l -> fin;

			l -> fin = nuevoNodo;

		}

		l -> tamanioLista++;

	}

}

bool estaVaciaChar(listaChar *l) {

	if (l != NULL) {

		return (obtenerCantidadChar(l) == 0);

	}

	return false;

}

int obtenerCantidadChar(listaChar *l) {

	if (l != NULL) {

		return l -> tamanioLista;

	}

	return -1;

}

nodoChar *obtenerPrimerChar(listaChar *l) {

	if (l != NULL) {

		return l -> inicio;

	}

	return NULL;

}


void imprimirListaChar(listaChar *l) {


	printf("\n====Lista====\n\n");

	if (obtenerCantidadChar(l) > 0) {
		
		nodoChar *puntero;

		int nChar;

		nChar = 1;

		for(puntero = obtenerPrimerChar(l); puntero != NULL; puntero = puntero -> siguiente) {

			char *cont;

			cont = puntero -> contenido;

			printf("\t%d). ", nChar);

			printf("%s\n", cont );

			nChar++;

		}

		//printf("\t%d Salir\n", nDevice++);

	}

	else {

		printf("\tLista Vacia\n");

		// printf("\n\t%d). Salir\n", obtenerCantidadContactos(contactos) + 1);

	}

}


char * buscarChar(listaChar *l, int numeroChar) {

	int i = 1;

	nodoChar *ptr;

	for (ptr = obtenerPrimerChar(l); ptr != NULL; ptr = ptr -> siguiente) {

		if (i == numeroChar) {

			return ptr -> contenido;

		}

		i++;

	}

	return NULL;

}