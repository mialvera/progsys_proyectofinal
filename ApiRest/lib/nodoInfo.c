#include <assert.h>
#include <nodoInfo.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <jWrite.h>
#include <jRead.h>

void crearListaInfo(listaInfo *l, int tamanioElemento, liberarEspacioInfo liberar)
{
	assert(tamanioElemento > 0);

	l -> tamanioLista = 0;

	l -> tamanioElemento = tamanioElemento;

	l -> inicio = l -> fin = NULL;

	l -> liberar = liberar;

}

nodoInfo * crearNodoInfo(listaInfo *l, Info *i) {

	if (l != NULL) {

		nodoInfo *nuevoNodo = (nodoInfo *) malloc(sizeof(nodoInfo));

		nuevoNodo -> info = malloc(l -> tamanioElemento);

		memcpy(nuevoNodo -> info, i, l -> tamanioElemento);

		return nuevoNodo;

	}

	return NULL;

}


void insertarInfoAlFinal(listaInfo *l, Info *i)
{
	if (l != NULL) {

		nodoInfo *nuevoNodo = crearNodoInfo(l, i);

		if (estaVaciaInfo(l)) {

			l -> inicio = l -> fin = nuevoNodo;

		}

		else {

			l -> fin -> siguiente = nuevoNodo;

			nuevoNodo -> previo = l -> fin;

			l -> fin = nuevoNodo;

		}

		l -> tamanioLista++;

	}

}

bool estaVaciaInfo(listaInfo *l) {

	if (l != NULL) {

		return (obtenerCantidadInfo(l) == 0);

	}

	return false;

}

int obtenerCantidadInfo(listaInfo *l) {

	if (l != NULL) {

		return l -> tamanioLista;

	}

	return -1;

}

nodoInfo *obtenerPrimerInfo(listaInfo *l) {

	if (l != NULL) {

		return l -> inicio;

	}

	return NULL;

}


void imprimirListaInfo(listaInfo *l) {


	printf("\n====Archivos====\n\n");

	if (obtenerCantidadInfo(l) > 0) {
		
		nodoInfo *puntero;

		int nInfo;

		nInfo = 1;

		for(puntero = obtenerPrimerInfo(l); puntero != NULL; puntero = puntero -> siguiente) {

			Info *i;

			i = puntero -> info;

			printf("\t%d).\n", nInfo);

			printInfo(i);

			nInfo++;

		}

	}

	else {

		printf("\tNo existen archivos para presentar\n");

		// printf("\n\t%d). Salir\n", obtenerCantidadContactos(contactos) + 1);

	}

}

char * escribirJSONInfo(listaInfo *l, char *file) {

	nodoInfo *ptr;

	char *buffer = (char *)calloc(102400, sizeof(char)); ;
	unsigned int buflen= 102400;
	int err;

	jwOpen(buffer, buflen, JW_OBJECT, JW_PRETTY);	
	
	jwObj_array("Archivos");

	for (ptr = obtenerPrimerInfo(l); ptr != NULL; ptr = ptr -> siguiente) {

		Info *i;

		i = ptr -> info;

		jwArr_object();

			jwObj_string("Nombre", i -> nombre);
			jwObj_string("Tamanio", i -> tamanio);

		jwEnd();

	}
	
	jwEnd();

	int status;
	char *mensaje;

	mensaje = (char *)calloc(6, sizeof(char));

	if (obtenerCantidadInfo(l) > 0) {
		status = 0;
		strcat(mensaje, "OK");
	}
	else {
		status = 1;
		strcat(mensaje, "ERROR");
	}

	jwObj_int( "status", status );
	jwObj_string( "str_error", mensaje);


	err = jwClose();

	//FILE *fp = fopen (file, "w+");
	//if (!fp) {
        //fprintf (stderr, "jsonout() error: file open failed '%s'.\n", 
         //       file);
    //}

    //fprintf (fp,"%s", buffer);
    // printf("A JSON file created: %s\n\n", file);

    return buffer;

}

listaInfo * leerJSONInfo(char *nombreArchivo) {

	struct FileBufferInfo json;
	struct jReadElement arrayElement;

	int i;
	char *pJsonArray;

	if( readFileBufferInfo( nombreArchivo, &json, FILE_BUFFER ) == 0 )
	{
		printf("No se puede abrir el archivo: %s\n\n", nombreArchivo );

		return NULL;
	}

	//printf("\n====Dispositivos cargados correctamente====\n\n");


	// Lee el JSON
	char * infoJSON = (char *)json.data;

	jRead( infoJSON, "{'Archivos'", &arrayElement );

	// printf("%s\n", infoJSON);

	if (arrayElement.elements > 0) {


		listaInfo *lista;

    	lista = (listaInfo *)malloc(2 * sizeof(listaInfo));

    	crearListaInfo(lista, sizeof(Info), NULL);

		char * valor = (char *)arrayElement.pValue;

		// printf("%s\n", valor);

		for(i = 0; i < arrayElement.elements; i++) {

			//printf("HERE!\n");

			char str[MAXIMO];

			Info *inf;

			inf = (Info *) malloc(sizeof(Info));

			// index the array using queryParam
			char *nom = (char *)malloc(MAXIMO);
			jRead_string( valor, "[*{'Nombre'", nom, MAXIMO, &i );
			inf -> nombre =  nom;
			//printf(d-> SubSystem);

			char *tam = (char *)malloc(MAXIMO);
			jRead_string( valor, "[*{'Tamanio'", tam, MAXIMO, &i );
			inf -> tamanio = tam;

			insertarInfoAlFinal(lista, inf);

		}

		return lista;

	}

	return NULL;

}


Info * buscarInfo(listaInfo *l, int numeroInfo) {

	int i = 1;

	nodoInfo *ptr;

	for (ptr = obtenerPrimerInfo(l); ptr != NULL; ptr = ptr -> siguiente) {

		if (i == numeroInfo) {

			return ptr -> info;

		}

		i++;

	}

	return NULL;

}

unsigned long readFileBufferInfo(char *filename, FileBufferInfo *pbuf, unsigned long maxlen) {
  	
  	FILE *fp;
  	int i;

	if( (fp=fopen(filename, "rb")) == NULL )
	{
		// printf("Can't open file: %s\n", filename );
		return 0;
	}
	// find file size and allocate buffer for JSON file
	fseek(fp, 0L, SEEK_END);
	pbuf->length = ftell(fp);
	if( pbuf->length >= maxlen )
	{
		fclose(fp);
		return 0;
	}
	// rewind and read file
	fseek(fp, 0L, SEEK_SET);
	pbuf->data= (unsigned char *)malloc( pbuf->length + 1 );
	memset( pbuf->data, 0, pbuf->length+1 );	// +1 guarantees trailing \0

	i= fread( pbuf->data, pbuf->length, 1, fp );	
	fclose( fp );
	if( i != 1 )
	{
		freeFileBufferInfo( pbuf );
		return 0;
	}
	return pbuf->length;

}

void freeFileBufferInfo(FileBufferInfo *buf) {
	if( buf->data != NULL )
		free( buf->data );
	buf->data= 0;
	buf->length= 0;
}
