#include <Info.h>
#include <stdio.h>
#include <string.h>

Info * crearInfo(char * informacion) {

	Info *info = (Info * )malloc(sizeof(Info));

	setDatosInfo(info, informacion);

	return info;
}


void setDatosInfo(Info * i, char * informacion){

    char *token;

    token = strtok(informacion, "|");

    char *nombre = (char *)malloc(strlen(informacion));

    char*tamanio = (char *)malloc(strlen(informacion));

    int count = 0;

    while(token) {

        if(count == 0){
            //memset(tamanio, '\0', sizeof(tamanio));
            strcpy(tamanio, token);
            tamanio[strlen(tamanio)] = '\0';
            //printf("%s\n", tamanio);
            i -> tamanio = tamanio;
        }

        if(count == 1){
            //memset(nombre, '\0', sizeof(nombre));
            strcpy(nombre, token);
            nombre[strlen(nombre) - 1] = '\0';
            //printf("%s\n",nombre );
            i -> nombre = nombre;
        }
        token = strtok(NULL, "|");

        count++;
    }

    //strcpy(i -> tamanio, result[0]);
    
    // strcpy(i -> nombre, result[1]);
    
    //i -> tamanio = tamanio;
    
    //i -> nombre = nombre;
    //free(result);

    //printInfo(i);
}

void printInfo(Info *i){

	printf("\tNombre: %s Tamaño: %s\n", i ->nombre, i -> tamanio );
}