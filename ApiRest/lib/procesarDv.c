#include <procesarDv.h>


char *getMontaje (char * dev_path) {

    FILE * mtab = NULL;
    struct mntent * part = NULL;
    char *montaje = (char * )calloc(100, sizeof(char));
 

    if ( ( mtab = setmntent ("/etc/mtab", "r") ) != NULL) {

    	while ( ( part = getmntent ( mtab) ) != NULL) {

    		if ( ( part->mnt_fsname != NULL ) 
        		&& ( strncmp ( part->mnt_fsname, dev_path, 8) ) == 0 ) {

        	  	//printf("%s\n", part->mnt_fsname);
            	strcpy(montaje,part->mnt_dir);
              montaje[strlen(montaje)] = '\0';

       		}	

      	}

      	endmntent ( mtab);

    }

    return montaje;

}

void setup(char *inputBuffer, char **args) 
{
    const char s[1] = " ";
    char *token;
    token = strtok(inputBuffer, s);
    int i = 0;
    while( token != NULL)
    {
        args[i] = token;
        i++;
        token = strtok(NULL,s);
    }
    args[i] = NULL;
}


void *listarFilesDevice(char *montaje){

	char *comandos[100];
  
  char *listar = malloc(strlen("./lib/shell.sh ") + strlen(montaje) + 1);

  strcpy(listar, "./lib/shell.sh ");

  strcat(listar, montaje);

	//printf("%s\n", listar);

	pid_t pid;

	if((pid = Fork()) == 0){
			
		setup(listar, comandos);

		printf("Ejecucion de Comandos... ");

    execvp(comandos[0], comandos);
		
		// if(execvp(comandos[0], comandos) < 0) {

		// 	printf("No se ejecuto\n");
		// 	exit(0);
		// }
	}
  int status;
  Waitpid(-1, &status, 0);
  //sleep(2);

  return NULL;
}

void *borrarError(char *filename) {

  char *comandos[100];
  
  char *listar = malloc(strlen("./lib/shell.sh ") + strlen(filename) + 1);

  strcpy(listar, "./lib/shell.sh ");

  strcat(listar, filename);

  // printf("%s\n", listar);

  pid_t pid;

  if((pid = Fork()) == 0){
      
    setup(listar, comandos);

    printf("Ejecucion de Comandos... ");
    
    execvp(comandos[0], comandos);
    
    

		// Write(connfd, mensaje , strlen(mensaje) + 1);


    // if(execvp(comandos[0], comandos) < 0) {

    //   printf("No se ejecuto\n");
    //   exit(0);
    // }
  }

  char mensaje[6];

  int status;
		if (waitpid(-1, &status, 0) < 0) {
			strcpy(mensaje, "ERROR");
		}
		else {
			strcpy(mensaje, "OK");
		}

  // sleep(1);

  return NULL;

}

void *borrarFile(char *montaje, char *fileName){

  char *comandos[100];
  
  char *listar = malloc(strlen("./lib/shell.sh ") + strlen(montaje) + strlen(fileName) + 1);

  strcpy(listar, "./lib/shell.sh ");

  strcat(listar, montaje);

  strcat(listar, fileName);

  printf("%s\n", listar);

  pid_t pid;

  if((pid = Fork()) == 0){
      
    setup(listar, comandos);

    printf("Ejecucion de Comandos... ");
    
    execvp(comandos[0], comandos);
    // if(execvp(comandos[0], comandos) < 0) {

    //   printf("No se ejecuto\n");
    //   exit(0);
    // }
  }

  char mensaje[6];
  int status;
		if (waitpid(-1, &status, 0) < 0) {
			strcpy(mensaje, "ERROR");
		}
		else {
			strcpy(mensaje, "OK");
		}

  // sleep(1);

  return NULL;
}

  listaInfo * readFileChar(char * fileName){

  listaInfo *lista;

  lista = (listaInfo *)malloc(2 * sizeof(listaInfo));

  crearListaInfo(lista, sizeof(Info), NULL);

    
  FILE * fp;
  size_t len = 0;
  ssize_t read;
  char * line;


if( access(fileName, F_OK ) != -1 ) {  

    fp = fopen(fileName , "r");
    //if (fp == NULL)     
  
    Info *info;
    int count = 0;

    while ((read = getline(&line, &len, fp)) != -1) {
      //Info *info;

        //line = (char *) malloc(read);
        //printf("Retrieved line of length %zu :\n", read);
        if(count > 0){

          info = crearInfo(line);
          insertarInfoAlFinal(lista, info);
        }

        count++;
        //printf(info);
    }

    fclose(fp);

  }
  // if (line)
  //     free(line);

  return lista;

}

void crearFile(char * montaje, char * file, char * texto){

  char *comandos[100];
  
  char orden[strlen(texto) + strlen(file) + strlen(montaje) + 11];

  strcpy(orden, "./lib/shell.sh ");
  
  strcat(orden, montaje);

  strcat(orden, " ");

  strcat(orden, file);

  strcat(orden, " \"");

  strcat(orden, texto);

  strcat(orden, "\"");
  
  printf("%s\n", orden);

  system(orden);

 
}