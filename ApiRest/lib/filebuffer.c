#include <filebuffer.h>

unsigned long readFileBuffer(char *filename, FileBuffer *pbuf, unsigned long maxlen) {
  	
  	FILE *fp;
  	int i;

	if( (fp=fopen(filename, "rb")) == NULL )
	{
		// printf("Can't open file: %s\n", filename );
		return 0;
	}
	// find file size and allocate buffer for JSON file
	fseek(fp, 0L, SEEK_END);
	pbuf->length = ftell(fp);
	if( pbuf->length >= maxlen )
	{
		fclose(fp);
		return 0;
	}
	// rewind and read file
	fseek(fp, 0L, SEEK_SET);
	pbuf->data= (unsigned char *)malloc( pbuf->length + 1 );
	memset( pbuf->data, 0, pbuf->length+1 );	// +1 guarantees trailing \0

	i= fread( pbuf->data, pbuf->length, 1, fp );	
	fclose( fp );
	if( i != 1 )
	{
		freeFileBuffer( pbuf );
		return 0;
	}
	return pbuf->length;

}

void freeFileBuffer(FileBuffer *buf) {
	if( buf->data != NULL )
		free( buf->data );
	buf->data= 0;
	buf->length= 0;
}