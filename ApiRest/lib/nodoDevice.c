#include <assert.h>
#include <nodoDevice.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
//#include <jWrite.h>


void crearListaDevices(listaDevices *l, int tamanioElemento, liberarEspacio liberar)
{
	assert(tamanioElemento > 0);

	l -> tamanioLista = 0;

	l -> tamanioElemento = tamanioElemento;

	l -> inicio = l -> fin = NULL;

	l -> liberar = liberar;

}

nodoDevice * crearNodoDevice(listaDevices *l, Device *d) {

	if (l != NULL) {

		nodoDevice *nuevoNodo = (nodoDevice *) malloc(sizeof(nodoDevice));

		nuevoNodo -> device = malloc(l -> tamanioElemento);

		memcpy(nuevoNodo -> device, d, l -> tamanioElemento);

		return nuevoNodo;

	}

	return NULL;

}


void insertarDeviceAlFinal(listaDevices *l, Device *d)
{
	if (l != NULL) {

		nodoDevice *nuevoNodo = crearNodoDevice(l, d);

		if (estaVaciaDevices(l)) {

			l -> inicio = l -> fin = nuevoNodo;

		}

		else {

			l -> fin -> siguiente = nuevoNodo;

			nuevoNodo -> previo = l -> fin;

			l -> fin = nuevoNodo;

		}

		l -> tamanioLista++;

	}

}

bool estaVaciaDevices(listaDevices *l) {

	if (l != NULL) {

		return (obtenerCantidadDevices(l) == 0);

	}

	return false;

}

int obtenerCantidadDevices(listaDevices *l) {

	if (l != NULL) {

		return l -> tamanioLista;

	}

	return -1;

}

nodoDevice *obtenerPrimerDevice(listaDevices *l) {

	if (l != NULL) {

		return l -> inicio;

	}

	return NULL;

}


void imprimirListaDevices(listaDevices *l) {


	printf("\n====Devices Disponibles====\n\n");

	if (obtenerCantidadDevices(l) > 0) {
		
		nodoDevice *puntero;

		int nDevice;

		nDevice = 1;

		for(puntero = obtenerPrimerDevice(l); puntero != NULL; puntero = puntero -> siguiente) {

			Device *d;

			d = puntero -> device;

			printf("\t%d).\n", nDevice);

			printDevice(d);

			nDevice++;

		}

		printf(" Final\n");

	}

	else {

		printf("\tNo existen contactos para presentar\n");

		// printf("\n\t%d). Salir\n", obtenerCantidadContactos(contactos) + 1);

	}

}

void escribirJSON(listaDevices *l, char *file) {

	nodoDevice *ptr;

	//int count =1;

	char buffer[102400];
	unsigned int buflen= 102400;
	int err;

	// printf("A JSON file created:\n\n" );

	jwOpen(buffer, buflen, JW_OBJECT, JW_PRETTY);	
	
	jwObj_array("Devices");

	for (ptr = obtenerPrimerDevice(l); ptr != NULL; ptr = ptr -> siguiente) {

		Device *d;

		d = ptr -> device;

		jwArr_object();

			jwObj_string("SubSystem", strdup(d->SubSystem));
			jwObj_string("fabricador", strdup(d->fabricante));
			jwObj_string("tipo", strdup(d->tipo));
			jwObj_string("accion", strdup(d->accion));
			jwObj_string("VI", strdup(d->VI));
			jwObj_string("PI", strdup(d->PI));
			jwObj_string("nodo", strdup(d->nodo));

		//count++;
		jwEnd();

	}

	jwEnd();

	int status;
	char *mensaje;

	mensaje = (char *)calloc(6, sizeof(char));

	if (obtenerCantidadDevices(l) > 0) {
		status = 0;
		strcat(mensaje, "OK");
	}
	else {
		status = 1;
		strcat(mensaje, "ERROR");
	}

	jwObj_int( "status", status );
	jwObj_string( "str_error", mensaje);

	err = jwClose();

	FILE *fp = fopen (file, "w+");
	if (!fp) {
        fprintf (stderr, "jsonout() error: file open failed '%s'.\n", 
                file);
    }

    fprintf (fp,"%s", buffer);
    printf("A JSON file created: %s\n\n", file);

}

unsigned long readFileBuffer(char *filename, FileBuffer *pbuf, unsigned long maxlen) {
  	
  	FILE *fp;
  	int i;

	if( (fp=fopen(filename, "rb")) == NULL )
	{
		// printf("Can't open file: %s\n", filename );
		return 0;
	}
	// find file size and allocate buffer for JSON file
	fseek(fp, 0L, SEEK_END);
	pbuf->length = ftell(fp);
	if( pbuf->length >= maxlen )
	{
		fclose(fp);
		return 0;
	}
	// rewind and read file
	fseek(fp, 0L, SEEK_SET);
	pbuf->data= (unsigned char *)malloc( pbuf->length + 1 );
	memset( pbuf->data, 0, pbuf->length+1 );	// +1 guarantees trailing \0

	i= fread( pbuf->data, pbuf->length, 1, fp );	
	fclose( fp );
	if( i != 1 )
	{
		freeFileBuffer( pbuf );
		return 0;
	}
	return pbuf->length;

}

void freeFileBuffer(FileBuffer *buf) {
	if( buf->data != NULL )
		free( buf->data );
	buf->data= 0;
	buf->length= 0;
}

listaDevices * leerJSON(char *nombreArchivo) {

	struct FileBuffer json;
	struct jReadElement arrayElement;
	// char *query= "[*{'Users'";
	int i;
	char *pJsonArray;
	// DWORD tStart, tEnd;
	// double elapsed;
	// int *UserCounts;

	if( readFileBuffer( nombreArchivo, &json, FILE_BUFFER_MAXLEN ) == 0 )
	{
		printf("No se puede abrir el archivo: %s\n\n", nombreArchivo );

		return NULL;
	}

	printf("\n====Dispositivos cargados correctamente====\n\n");


	// testQuery((char *)json.data , "{'contactos' *[", arrayElement);
	// for( i=0; i<2; i++ )

	// Lee el JSON
	char * infoJSON = (char *)json.data;

	jRead( infoJSON, "{'Devices'", &arrayElement );

	// printf("%s\n", infoJSON);

	if (arrayElement.elements > 0) {


		listaDevices *deviceslist;

    	deviceslist = (listaDevices *)malloc(2 * sizeof(listaDevices));

    	crearListaDevices(deviceslist, sizeof(Device), NULL);

		char * valor = (char *)arrayElement.pValue;

		// printf("%s\n", valor);

		for(i = 0; i < arrayElement.elements; i++) {

			//printf("HERE!\n");

			char str[MAXIMO];

			Device *d;

			d = (Device *) malloc(sizeof(Device));

			// index the array using queryParam
			char *subs = (char *)malloc(MAXIMO);
			jRead_string( valor, "[*{'SubSystem'", subs, MAXIMO, &i );
			d-> SubSystem =  subs;
			//printf(d-> SubSystem);

			char *fab = (char *)malloc(MAXIMO);
			jRead_string( valor, "[*{'fabricador'", fab, MAXIMO, &i );
			d->fabricante = fab;
			//printf(d->fabricante);

			char *tipo = (char *)malloc(MAXIMO);
			jRead_string( valor, "[*{'tipo'", tipo, MAXIMO, &i );
			d->tipo =  tipo;
			//printf(d->tipo);

			char *acc = (char *)malloc(MAXIMO);
			jRead_string( valor, "[*{'accion'", acc, MAXIMO, &i );
			d->accion = acc;
			//printf(d->accion);

			char *vi = (char *)malloc(MAXIMO);
			jRead_string( valor, "[*{'VI'", vi, MAXIMO, &i );
			d->VI = vi;
			//printf(d->VI);

			char *pi = (char *)malloc(MAXIMO);
			jRead_string( valor, "[*{'PI'", pi, MAXIMO, &i );
			d->PI = pi;
			//printf(d->PI);

			char *nodo = (char *)malloc(MAXIMO);
			jRead_string( valor, "[*{'nodo'", nodo, MAXIMO, &i );
			d->nodo = nodo;
			//printf(d->nodo);

			insertarDeviceAlFinal(deviceslist, d);

			//printDevice(d);
			// mostrarInfoContacto(c);

		}

		//imprimirListaDevices(deviceslist);
		return deviceslist;

		

	}

	// printf("JSON is array of %s elements\n\n", arrayElement);

	return NULL;

}


listaDevices * leerJSONFromString(char *infoJSON) {

	struct FileBuffer json;
	struct jReadElement arrayElement;
	// char *query= "[*{'Users'";
	int i;
	//char *pJsonArray;
	// DWORD tStart, tEnd;
	// double elapsed;
	// int *UserCounts;

	//if( readFileBuffer( nombreArchivo, &json, FILE_BUFFER_MAXLEN ) == 0 )
	//{
		//printf("No se puede abrir el archivo: %s\n\n", nombreArchivo );

		//return NULL;
	//}

	//printf("\n====Dispositivos cargados correctamente====\n\n");


	// testQuery((char *)json.data , "{'contactos' *[", arrayElement);
	// for( i=0; i<2; i++ )

	// Lee el JSON
	//char * infoJSON = (char *)json.data;

	jRead( infoJSON, "{'Devices'", &arrayElement );

	// printf("%s\n", infoJSON);

	if (arrayElement.elements > 0) {


		listaDevices *deviceslist;

    	deviceslist = (listaDevices *)malloc(2 * sizeof(listaDevices));

    	crearListaDevices(deviceslist, sizeof(Device), NULL);

		char * valor = (char *)arrayElement.pValue;

		// printf("%s\n", valor);

		for(i = 0; i < arrayElement.elements; i++) {

			//printf("HERE!\n");

			char str[MAXIMO];

			Device *d;

			d = (Device *) malloc(sizeof(Device));

			// index the array using queryParam
			char *subs = (char *)malloc(MAXIMO);
			jRead_string( valor, "[*{'SubSystem'", subs, MAXIMO, &i );
			d-> SubSystem =  subs;
			//printf(d-> SubSystem);

			char *fab = (char *)malloc(MAXIMO);
			jRead_string( valor, "[*{'fabricador'", fab, MAXIMO, &i );
			d->fabricante = fab;
			//printf(d->fabricante);

			char *tipo = (char *)malloc(MAXIMO);
			jRead_string( valor, "[*{'tipo'", tipo, MAXIMO, &i );
			d->tipo =  tipo;
			//printf(d->tipo);

			char *acc = (char *)malloc(MAXIMO);
			jRead_string( valor, "[*{'accion'", acc, MAXIMO, &i );
			d->accion = acc;
			//printf(d->accion);

			char *vi = (char *)malloc(MAXIMO);
			jRead_string( valor, "[*{'VI'", vi, MAXIMO, &i );
			d->VI = vi;
			//printf(d->VI);

			char *pi = (char *)malloc(MAXIMO);
			jRead_string( valor, "[*{'PI'", pi, MAXIMO, &i );
			d->PI = pi;
			//printf(d->PI);

			char *nodo = (char *)malloc(MAXIMO);
			jRead_string( valor, "[*{'nodo'", nodo, MAXIMO, &i );
			d->nodo = nodo;
			//printf(d->nodo);

			insertarDeviceAlFinal(deviceslist, d);

			//printDevice(d);
			// mostrarInfoContacto(c);

		}

		//imprimirListaDevices(deviceslist);
		return deviceslist;

		

	}

	// printf("JSON is array of %s elements\n\n", arrayElement);

	return NULL;

}

Device * buscarDevice(listaDevices *l, int numeroDevice) {

	int i = 1;

	nodoDevice *ptr;

	for (ptr = obtenerPrimerDevice(l); ptr != NULL; ptr = ptr -> siguiente) {

		if (i == numeroDevice) {

			return ptr -> device;

		}

		i++;

	}

	return NULL;

}