
#include <stdlib.h>
#include <device.h>
#include <stdio.h>

Device  *crearDevice(struct udev_device* dev){

	Device *d;

	d = (Device *) malloc(sizeof(Device));

	setDatos(d, dev);

	return d;

}

void setDatos(Device *d, struct udev_device* dev ){

	const char* action = udev_device_get_action(dev);
    if (! action)
        action = "exists";

    const char* vendor = udev_device_get_sysattr_value(dev, "idVendor");
    if (! vendor)
        vendor = "0000";

    const char* product = udev_device_get_sysattr_value(dev, "idProduct");
    if (! product)
        product = "0000";

    const char* nodo = udev_device_get_devnode(dev);
    if (! nodo)
        nodo = "null";

    const char* fabricante = udev_device_get_sysattr_value(dev,"manufacturer");
    if (! fabricante)
        fabricante = "null";

    d-> SubSystem = udev_device_get_subsystem(dev);

	d->fabricante = fabricante;

	d->tipo = udev_device_get_devtype(dev);

	d->accion = action;

	d->VI = vendor;

	d->PI = product;

	d->nodo = nodo;

}

void printDevice(Device *d){

	printf("SubSystem: %s\n\tFabricante: %s\n\tTipoDsptvo: %s \n\tAccion: %6s\n\tVI/PI: %s:%s\n\tNodo: %s\n\n",
        	d->SubSystem,
           	d->fabricante,
           	d->tipo,
           	d->accion,
           	d->VI,
           	d->PI,
           	d->nodo);

}