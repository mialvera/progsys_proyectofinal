#include <string.h>
#include <jansson.h>

#include <arpa/inet.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <jWrite.h>
#include <sys/mman.h>

#include <yder.h>
#include <orcania.h>

#define U_DISABLE_WEBSOCKET
#include "./lib/ulfius/src/ulfius.h"

#define SERVER_URL_PREFIX "http://localhost:2884/"

/**
 * decode a u_map into a string
 */
char * print_map(const struct _u_map * map) {
  char * line, * to_return = NULL;
  const char **keys;
  int len, i;
  if (map != NULL) {
    keys = u_map_enum_keys(map);
    for (i=0; keys[i] != NULL; i++) {
      len = snprintf(NULL, 0, "%s: %s\n", keys[i], u_map_get(map, keys[i]));
      line = o_malloc((len+1)*sizeof(char));
      snprintf(line, (len+1), "%s: %s\n", keys[i], u_map_get(map, keys[i]));
      if (to_return != NULL) {
        len = strlen(to_return) + strlen(line) + 1;
        to_return = o_realloc(to_return, (len+1)*sizeof(char));
      } else {
        to_return = o_malloc((strlen(line) + 1)*sizeof(char));
        to_return[0] = 0;
      }
      strcat(to_return, line);
      o_free(line);
    }
    return to_return;
  } else {
    return NULL;
  }
}

void print_response(struct _u_response * response) {
  if (response != NULL) {
    char * headers = print_map(response->map_header);
    char response_body[response->binary_body_length + 1];
    strncpy(response_body, response->binary_body, response->binary_body_length);
    response_body[response->binary_body_length] = '\0';
    printf("protocol is\n%s\n\n  headers are \n%s\n\n  body is \n%s\n\n",
           response->protocol, headers, response_body);

    //o_free(headers);
  }
}

int main (int argc, char **argv) {


  if (argc != 5)
  {
      printf("Usage: ./borrar <http_verb> <endpoint> <nodo> <archivo>\n");
      exit(0);
  }

  struct _u_map url_params;
  struct _u_response response;
  int res;
    
  u_map_init(&url_params);
  u_map_put(&url_params, "test", "one");
  u_map_put(&url_params, "other_test", "two");

  struct _u_request req_list;
  ulfius_init_request(&req_list);

  char nodo[50];
  strcpy(nodo, SERVER_URL_PREFIX);
  strcat(nodo, argv[2]);

  // Parameters in url
  req_list.http_verb = strdup(argv[1]);
  req_list.http_url = strdup(nodo);
  req_list.timeout = 20;
  u_map_copy_into(req_list.map_url, &url_params);

 
  /////////////////////////////////////////////////////////////////////////////
  
  char buffer[500];
  unsigned int buflen= 500;

  jwOpen(buffer, buflen, JW_OBJECT, JW_PRETTY); 

  jwObj_array("Solicitud");

    jwArr_object();

      jwObj_string("solicitud", "borrar");
      jwObj_string("nodo", argv[3]);
      jwObj_string("archivo", argv[4]);

    jwEnd();

  jwEnd();
  
  buffer[strlen(buffer)] = '\0';
  req_list.binary_body = strdup(buffer);
  req_list.binary_body_length = strlen(buffer) + 1;

  
  ulfius_init_response(&response);
  res = ulfius_send_http_request(&req_list, &response);
  if (res == U_OK) {
    print_response(&response);
  } else {
    printf("Error in http request: %d\n", res);
  }
  ulfius_clean_response(&response); 

  //////////////////////////////////////////////////////////////////////////////////////////////

  u_map_clean(&url_params);
  ulfius_clean_request(&req_list);

  
  return 0;

}
