# Proyecto II Parcial 2017 - I -> Programación de Sistemas

Monitoreo y control de dispositivos USB mediante un API REST.

Permite observar los dispositivos de almacenamiento masivo conectados a una computadora mediante un MonitorUSB y ApiRest, asi tambien procesos extra como:

Listar archivos que se encuentren dentro de un dispositivo
Eliminar un archivo especifico
Crear un archivo de texto dando como parámetro el texto que quisiera que haya en él

## Primeros Pasos...

El repositorio completo del MonitorUSB y ApiRest se encuentra aqui y puede descargarla para su uso

### Prerequisitos

Compilador GCC para Linux y Mac OsX
Git instalado y otras librerias.

```
$ sudo apt-get install libusb-1.0-0-dev
```
```
$ sudo apt-get install libudev-dev
```
```
$ sudo apt-get install libmicrohttpd-dev
```
```
$ sudo apt-get install libjansson-dev libcurl4-gnutls-dev libgnutls28-dev libgcrypt20-dev
```
 


### Instalando y Ejecutando

Hacer git clone del repositorio.

Para compilar el pograma antes debe descargar e intalar una libreria extra que se utilizo para la construccion de la Api, esta se deberia encontar en la carpeta /progsys_proyectofinal/ApiRest/lib/ulfius pero por motivos de que pertenece a otro git no se suben.

Los pasos a seguir son:

```
git clone https://bitbucket.org/mialvera/progsys_proyectofinal
```
En caso de no aparecer la libreria ulfius en la carpeta progsys_proyectofinal/ApiRest/lib y no se pueda compilar seguir los siguientes pasos, en caso contrario saltarse esto e ir a la parte de correr el programa.

```
$ cd progsys_proyectofinal/ApiRest/lib
```
```
$ git clone https://github.com/babelouest/ulfius.git
```
```
$ cd ulfius/
```
```
$ git submodule update --init
```
```
$ cd lib/orcania
```
```
$ make && sudo make install
```
```
$ cd ../yder
```
```
$ make && sudo make install
```
```
$ cd ../..
```
```
$ make
```
```
$ sudo make install
```
```
$ cd ../../..
```

Luego de esto para correr el programa ahora se debe seguir lo siguiente:

Correr el MonitorUSB

```
$ cd progsys_proyectofinal/pruebaUsbMonitor
```

```
$ make
```

```
$ ./mainMonitor
```

Correr el ApiRest (Como daemon, usar script restart.sh con el siguiente comando)

```
$ cd ../ApiRest
```

```
$ make
```

Ejecute este comando para correr el servidor

```
$ sudo ./restApi
```

**(Experimental)** (Puede contener errores) Considerése que ./restApi es el archivo ejecutable del servidor que también es **demonio**.
Con este comando se busca que el servidor no se caiga (a través de un script que también es **daemon**)
por lo cual se reiniciará en caso de que existan fallos.

```
$ sudo ./restart.sh setsid restart.sh > /dev/null 2>&1 < /dev/null
```

Para matar el proceso ./restApi

```
$ pgrep restApi
```

```
$ sudo kill -9 **idRestApi**
```

Considerése que idRestApi es obtenido a partir del comando que se ejecutó anteriormente antes de matarlo.

## Inicializando el programa

Al inciar el ApiRest en ella se encuentran creado los endpoints con sus respectivos Verbos HTTP

1. Para listar los dispositivos. No se necesita request
	HTTP-Verb: **GET**  Endpoint: **localhost:2884/devices**

2. Para listar los archivos. Se necesita request
	HTTP-Verb: **POST**  Endpoint: **localhost:2884/archivos**

3. Para borrar un arcchivo. Se necesita request
	HTTP-Verb: **DELETE**  Endpoint: **localhost:2884/archivos/eliminar**
	
4. Para crear un archivo. Se necesita request
	HTTP-Verb: **POST**  Endpoint: **localhost:2884/archivos/crear**

Para proceder a utilizar cada de uno de los procesos que ofrece se creo cuatro ejecutables.

* Para listar los dispositivos
	./devices
* Para listar archivos de un despositivo
	./listar <http_verb> <endpoint> <nodo>
* Para borrar un archivo de un dispositivo
	./borrar <http_verb> <endpoint> <nodo> <archivo>
* Para crear un archivo en un archivo
	./crear <http_verb> <endpoint> <nodo> <archivo>
	Luego le pedira el texto con el que quiera crear el archivo.

## Detalles de los HTTP VERBS y ENDPOINTS elegidos

- Se consideró oportuno tener un callback independiente para cada acción a realizar, por esa razón se tiene un endpoint diferente para cada proceso.

- También se consideró utilizar **DELETE** para para eliminar un archivo, en vez de **POST**, dado que su ejecución es más **segura**.

- Listar archivos solo admite **máximo** 30 archivos.

## API REST

- Para entender con claridad lo que realiza cada parte del código fuente en el lado del servidor, ese necesario considerar las funciones que trabajan como
callbacks, puesto que son las encargadas del procesamiento de la información recibida y envían la respuesta al cliente de acuerdo a lo solicitado.

Los nombres de las funciones son:
```c
// CALLBACKS

/*
 * Trabajará con el endpoint /devices - METODO GET
 * RESPONSE -> HTTP CODE (200 - OK)
 */
int callback_devices (const struct _u_request * request, struct _u_response * response, void * user_data);

/*
 * Trabajará con el endpoint /archivos - METODO POST
 * RESPONSE -> HTTP CODE (200 - OK)
 */
int callback_archivos (const struct _u_request * request, struct _u_response * response, void * user_data);

/*
 * Trabajará con el endpoint /archivos/eliminar - METODO DELETE
 * RESPONSE -> HTTP CODE (404 -> Archivo no encontrado, Dispositivo no válido)
 * RESPONSE -> HTTP CODE (200 - OK -> Archivo eliminado con éxito)
 */
int callback_borrar (const struct _u_request * request, struct _u_response * response, void * user_data);

/*
 * Trabajará con el endpoint /archivos/crear - METODO POST
 * RESPONSE -> HTTP CODE (500 -> Dispositivo no válido)
 * RESPONSE -> HTTP CODE (200 - OK -> Archivo creado con éxito)
 */
int callback_crear (const struct _u_request * request, struct _u_response * response, void * user_data);
```

## ENDPOINTS EN API REST

- Más antes se definieron los endpoints sin tanto detalle. Aquí se aprecia cada uno de ellos, utilizando
la librería `ulfius` que implementa `libmicrohttpd`

```c
// ENDPOINTS

// /devices -> Listará los dispositivos conectados al ordenador
// /archivos -> Mostrará los archivos existentes (directorio raiz) en un dispositivo USB.
// /archivos/crear -> Creará un archivo en el dispositivo USB seleccionado
// /archivos/eliminar -> Eliminará un archivo que será buscado en el dispositivo USB.

ulfius_add_endpoint_by_val(&instance, "GET", NULL, "/devices", 0, &callback_devices, NULL);       

ulfius_add_endpoint_by_val(&instance, "POST", NULL, "/archivos", 0, &callback_archivos, NULL);

ulfius_add_endpoint_by_val(&instance, "POST", NULL, "/archivos/crear", 0, &callback_crear, NULL);

ulfius_add_endpoint_by_val(&instance, "DELETE", NULL, "/archivos/eliminar", 0, &callback_borrar, NULL);
```

## Despliegue

Para poder acceder desde otra commputadora a sus dispositicos desde la misma red, cambiar la constante en cada una de los archivos devices.c , listar.c , borrar.c  y crear.c por SERVER_URL_PREFIX a http://ip-direction:2884/

## Elaborado con

* [GCC](https://gcc.gnu.org) - Compilador
* [Sublime Text](https://www.sublimetext.com/3) - El editor de texto usado
* [Visual Studio Code](https://code.visualstudio.com) - Otro editor de texto utilizado
* [Bitbucket](https://bitbucket.org) - Usado para guardar y compartir archivos

## Autores

* **Jose Masson** - *Initial work* - [Correo: jlmasson@espol.edu.ec]
* **Milton Vera** - *Initial work* - [Correo: mialvera@espol.edu.ec]











