/*
 compilar: gcc -o usbmonitor usbmonitor.c -ludev
 sudo apt-get install libusb-1.0-0-dev
 sudo apt-get install libudev-dev
*/

#include <usbmonitor.h>
#include <stdio.h>
#include <string.h>

#include <stdarg.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include <syslog.h>
#include <fcntl.h>
#include <sys/epoll.h>


#define NUM_OF_EVENTS 5
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))


#define SUBSYSTEM "usb"

void print_device(struct udev_device* dev)
{
    const char* action = udev_device_get_action(dev);
    if (! action)
        action = "exists";

    const char* vendor = udev_device_get_sysattr_value(dev, "idVendor");
    if (! vendor)
        vendor = "0000";

    const char* product = udev_device_get_sysattr_value(dev, "idProduct");
    if (! product)
        product = "0000";

    printf("SubSystem: %s\n\tFabricante: %s\n\tTipoDsptvo: %s \n\tAccion: %6s\n\tVI/PI: %s:%s\n\tNodo: %s\n\n",
           udev_device_get_subsystem(dev),
           udev_device_get_sysattr_value(dev,"manufacturer"),
           udev_device_get_devtype(dev),
           action,
           vendor,
           product,
           udev_device_get_devnode(dev));
}

void process_device(struct udev_device* dev)
{
    if (dev) {
        if (udev_device_get_devnode(dev))
            print_device(dev);

        udev_device_unref(dev);
    }
}


listaDevices *enumerate_devices(struct udev* udev)
{
    listaDevices *deviceslist;
    deviceslist = (listaDevices *)malloc(2 * sizeof(listaDevices));
    crearListaDevices(deviceslist, sizeof(Device), NULL);

   struct udev_enumerate* enumerate = udev_enumerate_new(udev);

    udev_enumerate_add_match_subsystem(enumerate, "scsi");
    udev_enumerate_add_match_property(enumerate, "DEVTYPE", "scsi_device");
    udev_enumerate_scan_devices(enumerate);

    struct udev_list_entry *devices = udev_enumerate_get_list_entry(enumerate);
    struct udev_list_entry *entry;

    Device *d;

    udev_list_entry_foreach(entry, devices) {
        const char* path = udev_list_entry_get_name(entry);
        struct udev_device* scsi = udev_device_new_from_syspath(udev, path);

        struct udev_device* block = get_child(udev, scsi, "block");
        struct udev_device* scsi_disk = get_child(udev, scsi, "scsi_disk");
        //struct udev_device* new = get_child(udev, block, "disk");
        struct udev_device* usb
            = udev_device_get_parent_with_subsystem_devtype(scsi, "usb", "usb_device");

        if (block && scsi_disk && usb) {

            
            d = crearDevice(usb, block);

            // printDevice(d);

            insertarDeviceAlFinal(deviceslist, d);

            //imprimirListaDevices(deviceslist);

            // printf("block = %s, usb = %s:%s, scsi = %s\n",
            //        udev_device_get_devnode(block),
            //        udev_device_get_sysattr_value(usb, "idVendor"),
            //        udev_device_get_sysattr_value(usb, "idProduct"),
            //        udev_device_get_sysattr_value(usb, "manufacturer")
            //        );
        }

        // if (block) {
        //     udev_device_unref(block);
        // }

        // if (scsi_disk) {
        //     udev_device_unref(scsi_disk);
        // }

        // udev_device_unref(scsi);
    }

    imprimirListaDevices(deviceslist);
    udev_enumerate_unref(enumerate);
    return deviceslist;
}

void monitor_devices(struct udev* udev, listaDevices *lista)
{
        struct udev_device *dev;

        int fd_udev = -1;
        struct epoll_event ep_udev = { 0 };

        struct udev_monitor *mon;
        int fd_ep = -1;

        udev = udev_new();

        mon = udev_monitor_new_from_netlink(udev, "udev");
        udev_monitor_filter_add_match_subsystem_devtype(mon, "usb", "usb_device");
        udev_monitor_filter_add_match_subsystem_devtype(mon, "block", NULL);

        udev_monitor_enable_receiving(mon);

        /// Setup epoll
        fd_ep = epoll_create1(0);
        if (fd_ep < 0) {
                fprintf(stderr, "error creating epoll\n");
                exit(1);
        }

        fd_udev = udev_monitor_get_fd(mon);
        ep_udev.events = EPOLLIN;
        ep_udev.data.fd = fd_udev;
        if (epoll_ctl(fd_ep, EPOLL_CTL_ADD, fd_udev, &ep_udev) < 0) {
                fprintf(stderr, "fail to add fd to epoll\n");
                exit(1);
        }
        /// Polling loop for devices
        Device *d;
        while (1) {
                int fdcount;
                struct epoll_event ev[NUM_OF_EVENTS];
                int i = 0;

                fdcount = epoll_wait(fd_ep, ev, NUM_OF_EVENTS, -1);
                if (fdcount < 0) {
                        if (errno != EINTR){
                                fprintf(stderr, "error receiving uevent message: %m\n");
                        }
                        continue;
                }
                //printf("fdcount: %d\n", fdcount);

                for (i = 0; i < fdcount; i++) {
                        if (ev[i].data.fd == fd_udev && ev[i].events & EPOLLIN) {

                                dev = udev_monitor_receive_device(mon);
                                if (dev == NULL){
                                        continue;
                                }

                                if(strcmp(udev_device_get_devtype(dev), "partition") == 0){

                                    //Device *d = (Device * )malloc(sizeof(Device));
                                    //d = crearDevice(dev);

                                    //struct udev_device* usb
                                    //= udev_device_get_parent_with_subsystem_devtype(dev, "usb", "usb_device");

                                    //d -> VI =  udev_device_get_sysattr_value(dev, "idVendor");
                                    //d -> PI = udev_device_get_sysattr_value(dev, "idProduct");
                                    //d -> fabricante = udev_device_get_sysattr_value(dev,"manufacturer");
                                    // printDevice(d);

                                    // insertarDeviceAlFinal(lista, d);
                                    // imprimirListaDevices(lista);                                

                                //printf("\n   Action: %s\n", udev_device_get_action(dev));       // add or remove events
                                //printf("   Node: %s\n", udev_device_get_devnode(dev));
                                //printf("   Subsystem: %s\n", udev_device_get_subsystem(dev));
                                //printf("   Devtype: %s\n", udev_device_get_devtype(dev));
                                }


                                udev_unref(udev);
                        }
                }


        }
        udev_monitor_unref(mon);


}


struct udev_device*
get_child(struct udev* udev, struct udev_device* parent, const char* subsystem)
{
    struct udev_device* child = NULL;
    struct udev_enumerate *enumerate = udev_enumerate_new(udev);

    udev_enumerate_add_match_parent(enumerate, parent);
    udev_enumerate_add_match_subsystem(enumerate, subsystem);
    udev_enumerate_scan_devices(enumerate);

    struct udev_list_entry *devices = udev_enumerate_get_list_entry(enumerate);
    struct udev_list_entry *entry;

    udev_list_entry_foreach(entry, devices) {
        const char *path = udev_list_entry_get_name(entry);
        child = udev_device_new_from_syspath(udev, path);
        break;
    }

    udev_enumerate_unref(enumerate);
    return child;
}




