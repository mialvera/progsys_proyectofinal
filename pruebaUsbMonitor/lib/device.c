
#include <stdlib.h>
#include <device.h>
#include <stdio.h>

Device  *crearDevice(struct udev_device* usb, struct udev_device* block){

	Device *d;

	d = (Device *) malloc(sizeof(Device));

	setDatos(d, usb, block);

	return d;

}

void setDatos(Device *d, struct udev_device* dev, struct udev_device* block){

  const char* action = malloc(sizeof(const char*));
  const char* vendor = malloc(sizeof(const char*));
  const char* product = malloc(sizeof(const char*));
  const char* fabricante = malloc(sizeof(const char*));
  const char* nodo = malloc(sizeof(const char*));


	action = udev_device_get_action(dev);
  if (! action)
      action = "exists";

  vendor = udev_device_get_sysattr_value(dev, "idVendor");
  if (! vendor)
      vendor = "0000";

  product = udev_device_get_sysattr_value(dev, "idProduct");
  if (! product)
      product = "0000";

  nodo = udev_device_get_devnode(block);
    if (! nodo)
      nodo = "null";

  fabricante = udev_device_get_sysattr_value(dev,"manufacturer");
  if (! fabricante)
      fabricante = "null";

  d-> SubSystem = udev_device_get_subsystem(dev);

	d->fabricante = fabricante;

	d->tipo = udev_device_get_devtype(block);

	d->accion = action;

	d->VI = vendor;

	d->PI = product;

	d->nodo = nodo;

}

void printDevice(Device *d){

	printf("SubSystem: %s\n\tFabricante: %s\n\tTipoDsptvo: %s \n\tAccion: %6s\n\tVI/PI: %s:%s\n\tNodo: %s\n\n",
        	d->SubSystem,
           	d->fabricante,
           	d->tipo,
           	d->accion,
           	d->VI,
           	d->PI,
           	d->nodo);

}