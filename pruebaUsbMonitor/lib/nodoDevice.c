#include <assert.h>
#include <nodoDevice.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
//#include <jWrite.h>


void crearListaDevices(listaDevices *l, int tamanioElemento, liberarEspacio liberar)
{
	assert(tamanioElemento > 0);

	l -> tamanioLista = 0;

	l -> tamanioElemento = tamanioElemento;

	l -> inicio = l -> fin = NULL;

	l -> liberar = liberar;

}

nodoDevice * crearNodoDevice(listaDevices *l, Device *d) {

	if (l != NULL) {

		nodoDevice *nuevoNodo = (nodoDevice *) malloc(sizeof(nodoDevice));

		nuevoNodo -> device = malloc(l -> tamanioElemento);

		memcpy(nuevoNodo -> device, d, l -> tamanioElemento);

		return nuevoNodo;

	}

	return NULL;

}


void insertarDeviceAlFinal(listaDevices *l, Device *d)
{
	if (l != NULL) {

		nodoDevice *nuevoNodo = crearNodoDevice(l, d);

		if (estaVaciaDevices(l)) {

			l -> inicio = l -> fin = nuevoNodo;

		}

		else {

			l -> fin -> siguiente = nuevoNodo;

			nuevoNodo -> previo = l -> fin;

			l -> fin = nuevoNodo;

		}

		l -> tamanioLista++;

	}

}

bool estaVaciaDevices(listaDevices *l) {

	if (l != NULL) {

		return (obtenerCantidadDevices(l) == 0);

	}

	return false;

}

int obtenerCantidadDevices(listaDevices *l) {

	if (l != NULL) {

		return l -> tamanioLista;

	}

	return -1;

}

nodoDevice *obtenerPrimerDevice(listaDevices *l) {

	if (l != NULL) {

		return l -> inicio;

	}

	return NULL;

}


void imprimirListaDevices(listaDevices *l){


	printf("\n====Devices Disponibles====\n\n");

	if (obtenerCantidadDevices(l) > 0) {
		
		nodoDevice *puntero;

		int nDevice;

		nDevice = 1;

		for(puntero = obtenerPrimerDevice(l); puntero != NULL; puntero = puntero -> siguiente) {

			Device *d;

			d = puntero -> device;

			printf("\t%d).\n", nDevice);

			printDevice(d);

			nDevice++;

		}

		printf(" Final\n");

	}

	else {

		printf("\tNo existen contactos para presentar\n");

		// printf("\n\t%d). Salir\n", obtenerCantidadContactos(contactos) + 1);

	}

}

void escribirJSON(listaDevices *l, char *file) {

	nodoDevice *ptr;

	//int count =1;

	char buffer[102400];
	unsigned int buflen= 102400;
	int err;

	// printf("A JSON file created:\n\n" );

	jwOpen(buffer, buflen, JW_OBJECT, JW_PRETTY);	
	
	jwObj_array("Devices");

	for (ptr = obtenerPrimerDevice(l); ptr != NULL; ptr = ptr -> siguiente) {

		Device *d;

		d = ptr -> device;

		jwArr_object();

			jwObj_string("SubSystem", strdup(d->SubSystem));
			jwObj_string("fabricador", strdup(d->fabricante));
			jwObj_string("tipo", strdup(d->tipo));
			jwObj_string("accion", strdup(d->accion));
			jwObj_string("VI", strdup(d->VI));
			jwObj_string("PI", strdup(d->PI));
			jwObj_string("nodo", strdup(d->nodo));

		//count++;
		jwEnd();

	}

	jwEnd();
	
	int status;
	char *mensaje;

	mensaje = (char *)calloc(6, sizeof(char));

	if (obtenerCantidadDevices(l) > 0) {
		status = 0;
		strcat(mensaje, "OK");
	}
	else {
		status = 1;
		strcat(mensaje, "ERROR");
	}

	jwObj_int( "status", status );
	jwObj_string( "str_error", mensaje);	

	err = jwClose();

	FILE *fp = fopen (file, "w+");
	if (!fp) {
        fprintf (stderr, "jsonout() error: file open failed '%s'.\n", 
                file);
    }

    fprintf (fp,"%s", buffer);
    printf("A JSON file created: %s\n\n", file);

}

unsigned long readFileBuffer(char *filename, FileBuffer *pbuf, unsigned long maxlen) {
  	
  	FILE *fp;
  	int i;

	if( (fp=fopen(filename, "rb")) == NULL )
	{
		// printf("Can't open file: %s\n", filename );
		return 0;
	}
	// find file size and allocate buffer for JSON file
	fseek(fp, 0L, SEEK_END);
	pbuf->length = ftell(fp);
	if( pbuf->length >= maxlen )
	{
		fclose(fp);
		return 0;
	}
	// rewind and read file
	fseek(fp, 0L, SEEK_SET);
	pbuf->data= (unsigned char *)malloc( pbuf->length + 1 );
	memset( pbuf->data, 0, pbuf->length+1 );	// +1 guarantees trailing \0

	i= fread( pbuf->data, pbuf->length, 1, fp );	
	fclose( fp );
	if( i != 1 )
	{
		freeFileBuffer( pbuf );
		return 0;
	}
	return pbuf->length;

}

void freeFileBuffer(FileBuffer *buf) {
	if( buf->data != NULL )
		free( buf->data );
	buf->data= 0;
	buf->length= 0;
}