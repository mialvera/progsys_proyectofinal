#include <libudev.h>
#include <nodoDevice.h>
#include <csapp.h>
//#include <device.h>


//#include <jWrite.h>
/*
	Metodos utilizados para listar y leer dispositivos
*/
void print_device(struct udev_device* dev);
void process_device(struct udev_device* dev);
listaDevices * enumerate_devices(struct udev* udev);
void monitor_devices(struct udev* udev, listaDevices *lista);
struct udev_device* get_child(struct udev* udev, struct udev_device* parent, const char* subsystem);
//void escribirJSON(struct udev_list_entry* entry,struct udev_list_entry* devices,struct udev* udev);
