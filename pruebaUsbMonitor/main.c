#include <stdio.h>
#include "csapp.h"
#include <usbmonitor.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
//#include <nodoDevice.h>
//#include <pthread.h>

#define MAXLINE_SERVER 1024 * 1024
listaDevices *devices; 
struct udev *udev;


int main(int argc, char* argv[])
{

    //udev = udev_new();
    
    pid_t process_id = 0;
    pid_t sid = 0;

    // Crea un proceso hijo
    process_id = Fork();
    // Indication of fork() failure
    if (process_id < 0){

        printf("fork failed!\n");
        // Return failure in exit status
        exit(1);

    }
    // Detecta al proceso padre y lo termina
    if (process_id > 0)
    {
        printf("process_id of child process %d \n", process_id);
        // return success in exit status
        exit(0);
    }

    //unmask the file mode
    umask(0);
    //setea la nueva sesion al hijo
    sid = setsid();
    if(sid < 0)
    {
    // Return failure
    exit(1);
    }

    //Cerrar el terminal para que corra en background

    // Cerrar stdin. stdout and stderr
    // close(STDIN_FILENO);
    // close(STDOUT_FILENO);
    // close(STDERR_FILENO);
    //pthread_t monitorr, request;

    int listenfd;
    struct sockaddr_in clientaddr;
    char *port;
    char filename[MAXLINE_SERVER];
    char buffer[MAXLINE_SERVER];
    char *directorio;
    

    // if (argc != 2) {
    //     fprintf(stderr, "usage: %s <port>\n", argv[0]);
    //     exit(0);
    // }
    port = "9090";

    // devices = (listaDevices *)malloc(2 * sizeof(listaDevices));
    // crearListaDevices(devices, sizeof(Device), NULL);


    listenfd = Open_listenfd(port);

    // Pthread_create(&monitorr, NULL, monitoring, NULL);
    // Pthread_create(&request, NULL, requesting, NULL);
    // Pthread_join(monitorr, NULL);
    // Pthread_join(request, NULL);

    while(1)
    {
        struct sockaddr_storage serverStorage;
        socklen_t    addr_size   = sizeof serverStorage;
        int newSocket = accept(listenfd, (struct sockaddr*)&serverStorage, &addr_size);

        	if(Fork() == 0){
        		
        		close(STDIN_FILENO);
    			close(STDOUT_FILENO);
    			close(STDERR_FILENO);

        		udev = udev_new();
        		devices = enumerate_devices(udev);
    			//imprimirListaDevices(devices);
    			escribirJSON(devices, "dispositivos.json");
   				udev_unref(udev);

   				exit(0);

        	}
        
        int status;
        waitpid(-1, &status, 0);

        // sleep(2);

        //se lee el nombre del archivo enviado por el cliente y se lo almacena en filename
        int  get = read(newSocket, filename, MAXLINE_SERVER - 1);

        filename[get] = '\0';
        //fprintf(stdout, "%s\n", filename);

        //se crea la estructura stat con el archivo
        struct stat sbuf;

        directorio = "./";

        char *ruta;

        ruta = malloc(strlen(directorio) + strlen(filename) + 1);

        strcpy(ruta, directorio);
        strcat(ruta, filename);

        // sleep(5);


        if (stat(ruta, &sbuf) < 0) {

            write(newSocket, "-1", 2);
        }

        else{

            //si si existe el archivo se envia el tamaño del archivo al cliente
            sprintf(buffer, "%lu" ,sbuf.st_size);
            write(newSocket, buffer,strlen(buffer) );

            //se lee el contenido del .txt y se lo almacena en line 
            if(sbuf.st_size > 0){

                //devices = enumerate_devices(udev);
                struct FileBuffer json;
                char *infoJSON;

                readFileBuffer( ruta, &json, FILE_BUFFER_MAXLEN );
                infoJSON = (char *)json.data;

                //Se envia el contenido de dispositivos.json al cliente

                // Espera a que cargue toda la información en el JSON
                sleep(2);

                write(newSocket, infoJSON, sbuf.st_size + 1);
            }


        }



        close(newSocket);
    }

    close(listenfd);


}


    // pthread_t monitorr, request;

    // udev = udev_new();

    // if (!udev) {
    //     fprintf(stderr, "udev_new() failed\n");
    //     return 1;
    // }
    
    // //monitor_devices(udev, devices);
    // devices = enumerate_devices(udev);
    //monitor_devices(udev);

    // Pthread_create(&monitorr, NULL, monitoring, NULL);
    // Pthread_create(&request, NULL, requesting, NULL);
        
    // Pthread_join(monitorr, NULL);
    // Pthread_join(request, NULL);
    //imprimirListaDevices(devices);


    //monitor_devices(udev);
     //udev_unref(udev);
    // return 0;


// void *monitoring(void * argv){

//         monitor_devices(udev, devices);
// }



// void *requesting(void * argv){

//     listenfd = Open_listenfd(port);

//     while(1)
//     {
//         struct sockaddr_storage serverStorage;
//         socklen_t    addr_size   = sizeof serverStorage;
//         int newSocket = accept(listenfd, (struct sockaddr*)&serverStorage, &addr_size);

//         if(newSocket > 0 ){

//             if(Fork() == 0){
//                 //close(STDIN_FILENO);
//                 //close(STDOUT_FILENO);
//                 //close(STDERR_FILENO);

//                 udev = udev_new();
//                 //devices = enumerate_devices(udev);
//                 imprimirListaDevices(devices);
//                 printf("asdasdasd\n");
//                 escribirJSON(devices, "dispositivos.json");
//                 udev_unref(udev);

//                 exit(0);

//             }
//         }

//         sleep(5);

//         //se lee el nombre del archivo enviado por el cliente y se lo almacena en filename
//         int  get = read(newSocket, filename, MAXLINE_SERVER - 1);

//         filename[get] = '\0';
//         //fprintf(stdout, "%s\n", filename);

//         //se crea la estructura stat con el archivo
//         struct stat sbuf;

//         directorio = "./";

//         char *ruta;

//         ruta = malloc(strlen(directorio) + strlen(filename) + 1);

//         strcpy(ruta, directorio);
//         strcat(ruta, filename);

//         sleep(5);


//         if (stat(ruta, &sbuf) < 0) {

//             write(newSocket, "-1", 2);
//         }

//         else{

//             //si si existe el archivo se envia el tamaño del archivo al cliente
//             sprintf(buffer, "%d" ,sbuf.st_size);
//             write(newSocket, buffer,strlen(buffer) );

//             //se lee el contenido del .txt y se lo almacena en line 
//             if(sbuf.st_size >0){

//                 //devices = enumerate_devices(udev);
//                 struct FileBuffer json;
//                 readFileBuffer( ruta, &json, FILE_BUFFER_MAXLEN );
//                 char * infoJSON = (char *)json.data;

//                 sleep(2);

//                 write(newSocket, infoJSON, sbuf.st_size + 1);
//             }


//         }



//         close(newSocket);
//     }

//     close(listenfd);
//  }